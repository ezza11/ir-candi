# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .aspect import SentimentWordNet, set_review, aspect_evaluation

# ASPECTS 
building = []
ambience = []
price_service = []
sorrounding = []
merchant = []
# Create your views here.
response = {
        "building" : {
            "positive": 0.0,
            "negative": 0.0,
            "neutral": 0.0
        }, #positive, negative, neutral
        "ambience" : {
            "positive": 0.0,
            "negative": 0.0,
            "neutral": 0.0
        },
        "price_service" : {
            "positive": 0.0,
            "negative": 0.0,
            "neutral": 0.0
        },
        "surrounding" : {
            "positive": 0.0,
            "negative": 0.0,
            "neutral": 0.0
        },
        "merchant" : {
            "positive": 0.0,
            "negative": 0.0,
            "neutral": 0.0
        }
}

def index(request):
    with open('main/txt/cluster.txt', encoding="utf8") as noun:
        for line in noun:
            word = line.split()
            print(word)
            if (len(word) == 1):
                continue
            elif (word[1] == "1"):
                building.append(word[0])
            elif (word[1] == "2"):
                ambience.append(word[0])
            elif (word[1] == "3"):
                price_service.append(word[0])
            elif (word[1] == "4"):
                sorrounding.append(word[0])
            elif (word[1] == "5"):
                merchant.append(word[0])
            else :
                continue
    return render(request, 'index.html', response)


def show_aspect(request, id):
    # reset_all()
    # get txt objects from model
    sentencez = set_review("main/txt/" + id + ".txt") # file reviewnya
    dictionary = SentimentWordNet("main/txt/swnALL.txt") # file SWN nya
    # get clusternya

    score = aspect_evaluation(sentencez, building, ambience, price_service, sorrounding, merchant, dictionary)
    print(score)
    for i in score.keys():
        print(i)
        response[i]["positive"] = get_score(score, 0, i)
        response[i]["negative"] = get_score(score, 1, i)
        response[i]["neutral"] = get_score(score, 2, i)

    return render(request, 'result.html', response)


def get_score(score, attitude, i):
    print(score['building'])
    print(score[i])
    total = score[i][0] + score[i][1] + score[i][2]
    return score[i][attitude] / float(total)

def result(request):
    return render(request, 'result.html', response)

