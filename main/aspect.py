import spacy
import json
import traceback

nlp = spacy.load('en_core_web_sm')

# Lists
sentences = []

# Aspects Flag
flag = ""


# Aspects Score
score = {
    "building" : [0,0,0], #positive, negative, neutral
    "ambience" : [0,0,0],
    "price_service" : [0,0,0],
    "surrounding" : [0,0,0],
    "merchant" : [0,0,0]
}

#FILES
swn_path = "swnALL.txt"

# SWN Dictionary
dictionary = {}


def SentimentWordNet(path_to_file):
    temp_dictionary = {}

    SWNfile = open(path_to_file, "r")
    try:
        for line in SWNfile:
            # if its a comment, skip this line
            if (line[0] != "#"):
                data = line.split("\t")
                wordTypeMarker = data[0]
                #Example line:
				#POS ID PosS NegS SynsetTerm#sensenumber Desc
				#a 00009618 0.5 0.25 spartan#4 austere#3 ascetical#2
				#ascetic#2 practicing great self-denial;...etc

			    #Is it a valid line? Otherwise, through exception.
                
                #Calculate synset score as score = PosS - NegS
                synsetScore = float(data[2]) - float(data[3])

                #Get all Synset terms
                synTermsSplit = data[4].split(" ")

                #Go through all terms of current synset.
                for synTermSplit in synTermsSplit:
                    # Get synterm and synterm rank
                    synTermAndRank = synTermSplit.split("#")
                    synTerm = synTermAndRank[0] + "#" + wordTypeMarker

                    synTermRank = int(synTermAndRank[1])

                    if(synTerm not in temp_dictionary):
                        temp_dictionary[synTerm] = {}

                    #Add synset link to synterm
                    temp_dictionary[synTerm] = {synTermRank : synsetScore}

        
        for i in temp_dictionary.keys(): 
            word = i
            synSetScoreMap = temp_dictionary[word]
            #Calculate weighted average. Weigh the synsets according to
			#their rank.
			#Score= 1/2*first + 1/3*second + 1/4*third ..... etc.
			#Sum = 1/1 + 1/2 + 1/3 ...
            score = 0.0
            sum_ = 0.0
            for j in synSetScoreMap.keys():
                score += synSetScoreMap[j] / float(j)
                sum_ += 1.0 / float(j)
            score /= sum_

            dictionary[word] = score
    
    except:
        traceback.print_exc()
    finally:
        SWNfile.close()

    return dictionary

def set_review(review):
    sentences = []
    with open(review,  encoding="utf8") as f:
        for line in f:
            sentences += line.split(".")
    return sentences


def aspect_evaluation(sentences, building, ambience, price_service, sorrounding, merchant, dicts):
    # print(building)
    # print(ambience)
    # print(price_service)
    # print(sorrounding)
    score = {
        "building" : [0,0,0], #positive, negative, neutral
        "ambience" : [0,0,0],
        "price_service" : [0,0,0],
        "surrounding" : [0,0,0],
        "merchant" : [0,0,0]
    } 
    flag = ""
    for sentence in sentences:
    #split space    
    # splitted_sentence = sentence.split(" ")
    #for i in splitted_sentence:
        preNER_word = sentence
        ner_word =  nlp(preNER_word)
        #ner.write(ner_word.text)
        sentence_orientation = 0
        for token in ner_word:
            text = token.text.lower()
            pos = token.pos_
            print(pos)
            if (text in building):
                flag = "building"
                print("bisa")
            elif (text in ambience):
                flag = "ambience"
            elif (text in price_service):
                flag = "price_service"
            elif (text in sorrounding):
                flag = "surrounding"
            elif (text in merchant):
                flag = "merchant"
            else:
                # method score
                query = ""
                if (pos == "NOUN"):
                    query = text + "#" + "n"
                elif (pos == "VERB"):
                    query = text + "#" + "v"
                elif (pos == "ADV"):
                    query = text + "#" + "r"
                elif (pos == "ADJ"):
                    query = text + "#" + "a"
                else:
                    continue
                
                if(query in dicts):
                    sentence_orientation += dicts[query]
        
        if(sentence_orientation > 0):
            score[flag][0] += 1
        elif(sentence_orientation < 0):
            score[flag][1] += 1
        else:
            score[flag][2] += 1

    return score

  

# def reset_all():
#     building = []
#     ambience = []
#     price_service = []
#     sorrounding = []
#     merchant = []

#     score = {
#     "building" : [0,0,0], #positive, negative, neutral
#     "ambience" : [0,0,0],
#     "price_service" : [0,0,0],
#     "surrounding" : [0,0,0],
#     "merchant" : [0,0,0]
#     }

#     sentences = []

#     flag = ""

