from django.conf.urls import url
from django.urls import path
from .views import index, show_aspect

urlpatterns = [
     path("", index, name='index'),
     url(r'^result/(?P<id>\w{0,50})/$', show_aspect, name='show_aspect')
     # path("result/(?P<id>\w{0,50})",show_aspect, name='result')
]