"""irCandi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.urls import path, include
from django.contrib import admin
from django.urls import path
import main.urls as main 
from django.views.generic import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('main/', include(main)),
    # url(r'^main/', include(main, namespace='main')),
    url(r'^main/', include(('main.urls', 'main'), namespace='main')),
    path('', RedirectView.as_view(
        url='/main/', permanent=True
    )),
]
